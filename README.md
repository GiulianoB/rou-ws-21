# rou-ws-21

## Tag 1

Voraussetzungen:
- PostgreSQL + PostGIS Installation
- QGIS Installation
- Installation von osm2pgsql 
    https://osm2pgsql.org/download/windows/
    https://learnosm.org/en/osm-data/osm2pgsql/

### Workflow

1. Dowonload OSM Daten
    https://download.geofabrik.de/europe/germany.html
    https://wiki.openstreetmap.org/wiki/Downloading_data#Download_the_data

2. Neue Datenbank erstellen: PGAdmin > Create Database.

3. Postgis und PGRouting Erweiterung installieren:
```
create extension postgis;
create extension pgrouting;
```

4. osm2pgsql
```
osm2pgsql -c -d hamburg -U postgres -H localhost -W -S C:\Users\giuliano\Downloads\default.style C:\Users\giuliano\Downloads\hamburg-latest.osm.pbf
```
- -d: Datenbankname aus Schritt 2
- -U: Postgres user
- -P: Port (falls der postgres service nicht 5432 läuft) 
- Default Style liegt im osm2pgsql Installationsverzeichnis

5. QGIS mit Postgres verbinden

6. Kantentabelle erstellen
```
drop table if exists edges;
create table edges as
select
	osm_id as id,
	null::bigint as source,
	null::bigint target,
	highway as road_type,
	way as geom
from planet_osm_roads
```

7. Topologie erzeugen
```
SELECT  pgr_createTopology('edges', 0.001, 'geom');
```
https://docs.pgrouting.org/latest/en/pgr_createTopology.html

8. Visualisier in Schritt 6 und 7 erzeugte Kanten und Knoten mit QGIS

### Aufgaben

- Schritt 1 - 8 aus Workflow von Tag 1 mit anderen Daten wiederholen
- Vergleiche OSM Daten (roads und points) mit Topologie Daten (edges und edges_vertices_pgr)
- Qualitätsanalyse
- Studiere und teste den Toleranzparameter der pgr_createTopology Funktion

## Tag 2

### Workflow

1. Erzeuge ein Test Kantentabelle
```
drop table if exists test_edges;
create table test_edges (
	id bigserial primary key,
	source bigint,
	target bigint,
	road_type text,
	geom geometry
);
```

2. Test Kantentabelle mit QGIS befüllen

3. Topologie erstellen
```
select pgr_createTopology(
	'test_edges',
	45,
	'geom',
	'id',
	'source',
	'target',
	rows_where := 'true', 
	clean := true
);
```

4. Topologie analysieren
```
select pgr_analyzeGraph('test_edges', 45, 'geom');
```

### Aufgaben
- Was bedeuten die Resultate der pgr_analyzeGraph Funktionen und wie beeinflussen diese unser Routing.
- Wie könnte man schlecht Topologieresultate verbessern.
- Mit Routing Algorithmen Dijkstra und A-Star vertraut machen. 


## Tag 3

### Workflow

1. Erzeuge ein Test Kantentabelle mit Feldern für eine die Straßentypen und einer Geschwindigkeitsbegrenzung
```
drop table if exists test_edges;
create table test_edges (
	id bigserial primary key,
	source bigint,
	target bigint,
	road_type text,
	speed_limit integer,
	geom geometry
);
```

2. Generiere mit QGIS ein Test- Kanten Netzwerk

3. Erzeuge anhand des Test- Kanten Netzwerk eine Topologie
```
SELECT  pgr_createTopology('test_edges', 0.001, 'geom');
```

4. Analysiere die Qualität der erzeugten Topologie
```
select pgr_analyzeGraph('test_edges', 45, 'geom');
```

5. Wende die einfachste Version des PGRouting Dijkstra Algorithmus auf deiner Topology an
```
SELECT * FROM pgr_dijkstra(
    'SELECT id, source, target, 1 as cost, 1 as reverse_cost FROM test_edges',
    1, 12
);
```

6. Modifiziere die SQL Query des PGRouting Dijkstra Algorithmus und beurteile die Ergebnise
```
SELECT * FROM pgr_dijkstra(
    'SELECT id, source, target, 1 as cost, 1 as reverse_cost FROM test_edges',
    1, 12
);
```

7. Baue ein SQL View, mit der Resultate aus der pgr_dijkstra funktion visualisiert werden können
```
CREATE OR REPLACE VIEW v_shortest_way_bw as
SELECT 
	b.edge, 
	a.source, 
	a.target, 
	a.geom 
FROM  pgr_dijkstra(
	'SELECT id, source, target ,st_length(geom) as cost , st_length(geom) as reverse_cost from edges',
	50539,
	124260
) b, edges a where b.edge = a.id;
```

### Aufgaben

- Was sind die Unterschiede von seq und path_seq der pgr_dijkstra Funktkionsresultate?
- was gibt der "reverse_cost" parameter der pgr_dijkstra Funktkion an?
- Beweise durch händische Berechnungen, dass die pgrouting ergebnisse richtig sind.
- Baue eine View, die alle Knoten des schnellsten Weges visualisiert.
- Teste ander PRouting Funktkionen.

## Tag 4

### Aufgaben

1. Schneide Netzwerkkanten mit Punkt Geometrien, die auf den Kanten liegen aber noch nicht Teil des Netzwerkes sind. Verwende dazu ein selbst erzeugtes Testnetzwerk
![](sources/cut-network.PNG)

2. Schreibe ein SQL Skript das mithilfe von PostGIS, Informationen aus nahegelegenen “Nicht“ - Netzwerkknoten in Netzwerkknoten überträgt. Verwende hierfür ein Testnetzwerk, das neben den Knoten- und Kantentabelle eine weitere Tabelle mit Punkt Geometrien beinhaltet. Die neue Tabelle soll außer den Punkt Geometrien auch noch sachliche Informationen / Spalten besitzen, die in die Netzwerkknoten überführt werden sollen.
![](sources/get-node-informations.jpg)

3. Bisher haben wir mithilfe einer SQL Query Kanten in unserm Netzwerk gefiltert, die wir für das Dijkstra Routing verwenden wollten. Schreibe eine SQL Query in der mithilfe von Spalten der Knotentabelle gefiltert werden kann.

4. Baue einen SQL Workflow um den schnellsten Weg von A nach B zu ermitteln unter Einbeziehung vom einem Start und Endpunkten die nicht im topologischen Knoten und Kanten Netzwerk liegen.
![](sources/start-end-point.jpg)

5. Führe Performance Vergleiche verschiedener PGR Routing Funktionen durch.